# onlyoffice.ide

Once booted, if you want to integrate with nextcloud, do the following in nextcloud:
- Go to apps
- Install onlyoffice app (search in the top right search bar)
- Set the values below, then click save, refresh
- You should be able to edit/create office files in nextcloud

Values in settings:

```
# Document Editing Service address = value in ONLYOFFICE_CONTAINER_VHOST variable of your .env file
https://onlyoffice.domain.local/

# Secret key (leave blank to disable) = value in ONLYOFFICE_SECRET_KEY variable of your .env file
yoursecrethere

# Advanced server settings 
# Document Editing Service address for internal requests from the server = value in ONLYOFFICE_CONTAINER_VHOST variable of your .env file
https://onlyoffice.domain.local/

Server address for internal requests from the Document Editing Service = value in NEXTCLOUD_CONTAINER_VHOST variable of your nextcloud .env file
https://nextcloud.domain.local/
```