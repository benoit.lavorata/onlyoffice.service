#!/bin/bash

set -x

export $(cat .env | xargs)
echo " get trusted domain list"
#docker exec -u www-data $NEXTCLOUD_CONTAINER_NAME php occ --no-warnings config:system:get trusted_domains >> trusted_domain.tmp

echo " add trusted domain list"
if ! grep -q "$NEXTCLOUD_CONTAINER_NAME" trusted_domain.tmp; then
    TRUSTED_INDEX=$(cat trusted_domain.tmp | wc -l);
    #docker exec -u www-data $NEXTCLOUD_CONTAINER_NAME php occ --no-warnings config:system:set trusted_domains $TRUSTED_INDEX --value="$NGINX_CONTAINER_NAME"
fi

#rm trusted_domain.tmp

docker exec -u www-data $NEXTCLOUD_CONTAINER_NAME php occ --no-warnings app:install onlyoffice
docker exec -u www-data $NEXTCLOUD_CONTAINER_NAME php occ --no-warnings config:system:set onlyoffice DocumentServerUrl --value="/ds-vpath/"
docker exec -u www-data $NEXTCLOUD_CONTAINER_NAME php occ --no-warnings config:system:set onlyoffice DocumentServerInternalUrl --value="http://$ONLYOFFICE_CONTAINER_NAME/"
docker exec -u www-data $NEXTCLOUD_CONTAINER_NAME php occ --no-warnings config:system:set onlyoffice StorageUrl --value="http://$NGINX_CONTAINER_NAME/"
