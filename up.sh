#!/bin/bash
echo ""
echo "Only Office Server"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $ONLYOFFICE_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name $ONLYOFFICE_CONTAINER_VOLUME_DATA
    
    FILE=".binded"
    echo ""
    if [ -f $FILE ]; then
        echo "Binding to nextcloud (container $NEXTCLOUD_CONTAINER_NAME)..."
        ./bind_nextcloud.sh
        touch .binded
    else
        echo "Already binded to nextcloud"
    fi
    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

